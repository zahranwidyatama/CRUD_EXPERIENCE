@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Daftar KTP</div>

                <div class="panel-body">
                    <form method="post" action="{{url('update')}}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{$masyarakat->id}}">
                <div class="form-group">
                    <label>NIK</label>
                    <input type="text" name= "nik" class="form-control" value="{{$masyarakat->nik}}">
                </div>
                 <div class="form-group">
                    <label>Name</label>
                    <input type="text" name= "nama" class="form-control" value="{{$masyarakat->nama}}">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label><br>
                    <input type="radio" name="jenis_kelamin" value="L">Laki-laki<br>
                    <input type="radio" name="jenis_kelamin" value="P">Perempuan
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tempat_lahir"  class="form-control" value="{{$masyarakat->tempat_lahir}}">  
                </div>
                <div class="form-group">
                    <label>Tanggal lahir</label>
                    <input type="text" name="tgl_lahir" value class="form-control" value="{{$masyarakat->tgl_lahir}}">
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="alamat" value class="form-control" value="{{$masyarakat->alamat}}">
                </div>
                <div class="form-group">
                    <label>RT/RW</label>
                    <input type="text" name="rt_rw" class="form-control" value="{{$masyarakat->rt_rw}}">
                </div>
                <div class="form-group">
                    <label>Kelurahan</label>
                    <input type="text" name="kelurahan" class="form-control" value="{{$masyarakat->kelurahan}}">
                </div>
                <div class="form-group">
                    <label>kecamatan</label>
                    <input type="text" name="kecamatan" class="form-control" value="{{$masyarakat->kecamatan}}">
                </div>
                <div class="form-group">
                    <label>Agama</label>
                    <select name="agama" class="form-control">
                     <option value="islam">Islam</option>
                     <option value="kristen">kristen</option>
                     <option value="budha">Budha</option>
                     <option value="hindu">Hindu</option>
                   </select>
                </div>
                <div class="form-group">
                    <label>Status perkawinan</label>
                    <input type="text" name="status_perkawinan" class="form-control" value="{{$masyarakat->status_perkawinan}}">
                </div>
                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" name="pekerjaan" class="form-control" value="{{$masyarakat->pekerjaan}}">
                </div>   
                <div class="form-group">
                    <label>Kewarganegaraan</label>
                    <input type="text" name="kewarganegaraan" class="form-control" value="{{$masyarakat->kewarganegaraan}}">
                </div>     
                <div class="form-group">
                    <label>Berlaku hingga</label>
                    <input type="text" name="berlaku_hingga" class="form-control" value="{{$masyarakat->berlaku_hingga}}">
                </div>     
                <div class="form-group">
                    <input type="submit" name="" class="btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
</div>

@endsection
