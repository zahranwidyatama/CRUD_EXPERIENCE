<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get("/kependudukan",'kependudukanController@index');
Route::get("/input_kependudukan", 'kependudukanController@add');
Route::get("/edit/{id}", 'kependudukanController@edit');
Route::get("/delete/{id}", 'kependudukanController@delete');


Route::post("/insert", "kependudukanController@insert");
Route::post("/update", "kependudukanController@update");
// Localhost/ktp/public/


