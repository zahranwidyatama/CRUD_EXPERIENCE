<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class penduduk extends Model
{
    protected $table ='penduduk';

    public $timestamps = true;

    protected $guarded = [];
}
